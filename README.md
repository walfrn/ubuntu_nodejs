# Docker Ubuntu NodeJS Angular/CL Typescrypt

Base Ubuutu:latest
NodeJS, AngularCLI and Typescript

# Objectives

* Facilitate use AngularCLI with ubuntu latest version

## How to use this image

```
Examples to start this container (linux):
docker run -it --name your_container ubuntu_nodejs
docker run -it -p 80:80 --name your_container ubuntu_nodejs
docker run -it --name your_container -v /$HOME/ndjs:/var/www/html ubuntu_nodejs


```
# Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.


## Authors

* [https://github.com/walfrn]

